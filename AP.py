import bpy

class MCN_OT_AP(bpy.types.Operator):
    """"""
    bl_idname = "object.add_cube"
    bl_label = "Default Cube Generator"  
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        
        name_cube = "Default Cube"
        #Select and delete all
        if bpy.context.object.mode == 'EDIT':
            bpy.ops.object.mode_set(mode='OBJECT')
        bpy.ops.object.select_all(action='SELECT')
        bpy.ops.object.delete(use_global=False)
        
        #Add cube and rename it
        bpy.ops.mesh.primitive_cube_add()
        for obj in bpy.context.scene.objects:
            obj.name = name_cube      
    
        return {'FINISHED'} 
    


class MCN_PT_AP(bpy.types.Panel):
    """"""
    bl_label = "AP"
    bl_idname = "MCN_PT_AP"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Tool"
    bl_parent_id = "MCN_PT_Main_Panel"
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        layout = self.layout

        # TODO Replace this label by your stuff here
        layout.label(text="AP add the default cube")
        layout.operator("object.add_cube", icon ="FUND")


def register():
    bpy.utils.register_class(MCN_PT_AP)
    bpy.utils.register_class(MCN_OT_AP)

def unregister():
    bpy.utils.unregister_class(MCN_PT_AP)
    bpy.utils.unregister_class(MCN_OT_AP)
    
#if __name__ == "__main__":
#    register()